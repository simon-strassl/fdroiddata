Categories:Office
License:GPLv2+
Web Site:
Source Code:https://github.com/LukeStonehm/LogicalDefence
Issue Tracker:https://github.com/LukeStonehm/LogicalDefence/issues

Auto Name:Logical Defence
Summary:Encyclopedia of logical fallacies
Description:
View a list of logical fallacies.
.

Repo Type:git
Repo:https://github.com/LukeStonehm/LogicalDefence

Build:1.0.1,2
    commit=b375b6d11e3fae55cad0fd0761ec261cc5b9e576
    subdir=app
    gradle=yes

Build:1.0.2,3
    commit=727db678d9c3f4459c2f94d4fdc7064031ea1714
    subdir=app
    gradle=yes

Build:1.0.3,4
    commit=c732d52560b525aaa9f50f621a117a8ea909c534
    subdir=app
    gradle=yes

Build:1.0.4,5
    commit=0fe4ad23caff1584b6857dc6d4192b5fda49ba87
    subdir=app
    gradle=yes

Build:1.0.5,6
    commit=b9b5768d10c776a611f291d7ecbd913a5fbb9455
    subdir=app
    gradle=yes

Build:1.0.6,7
    commit=c09823232456186457e97bbae5a9e12ef6b06e50
    subdir=app
    gradle=yes

Build:1.0.7,8
    commit=423d21efa0f2177d36c58a356155755df23b9821
    subdir=app
    gradle=yes

Build:1.1.0,9
    commit=1d3772ac5f161e52b78e83d204b3d8058fe972a2
    subdir=app
    gradle=yes

Build:1.1.1,10
    commit=5aec1436b009b87db1532b0a9ae68ff4d3546deb
    subdir=app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1.1
Current Version Code:10

