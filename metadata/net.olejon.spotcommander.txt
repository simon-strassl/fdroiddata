Categories:Multimedia
License:GPLv3
Web Site:https://olejon.github.io/spotcommander
Source Code:https://github.com/olejon/spotcommander
Issue Tracker:https://github.com/olejon/spotcommander/issues
Donate:http://www.olejon.net/code/spotcommander/?donate

Auto Name:SpotCommander
Summary:Remote control for Spotify for Linux
Description:
Remote control Spotify for Linux. For this to work, your have to install
[http://www.olejon.net/code/spotcommander/?install SpotCommander] on your
regular PC first.
.

Repo Type:git
Repo:https://github.com/olejon/spotcommander

Build:4.7,47
    commit=c468892825cc300889fcb105223e10f271745e98
    subdir=android/app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:4.7
Current Version Code:47

