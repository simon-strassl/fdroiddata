Categories:Security
License:Apache2
Web Site:https://code.google.com/p/derandom
Source Code:https://code.google.com/p/derandom/source
Issue Tracker:https://code.google.com/p/derandom/issues
Bitcoin:1NZz4TGpJ1VL4Qmqw7aRAurASAT3Cq5S6s

Auto Name:Derandom
Summary:Pseudo random number predictor
Description:
Predicts pseudo random numbers based on a sequence of observed numbers.
.

Repo Type:git
Repo:https://code.google.com/p/derandom/

Build:1.3,4
    commit=v1.3
    subdir=app
    gradle=yes

Build:1.4,5
    commit=v1.4
    subdir=app
    gradle=yes

Build:1.5,6
    commit=v1.5
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.5
Current Version Code:6

