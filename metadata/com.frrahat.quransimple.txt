Categories:Reading
License:GPLv2+
Web Site:
Source Code:https://github.com/frrahat/Quran-For-My-Android
Issue Tracker:https://github.com/frrahat/Quran-For-My-Android/issues

Auto Name:Quran For My Android
Summary:Shows Quran text or word by word translation
Description:
Displays Quran text or word by word translation.
.

Repo Type:git
Repo:https://github.com/frrahat/Quran-For-My-Android

Build:1.0,1
    commit=0bc23ac948a498ba74249caa58e433a6bfab80bb
    rm=libs/*
    extlibs=android/android-support-v4.jar
    prebuild=echo -e 'java.source=1.7\njava.target=1.7' > ant.properties

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.0
Current Version Code:1

