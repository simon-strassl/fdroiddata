Categories:Multimedia
License:GPLv3
Web Site:https://github.com/bottiger/SoundWaves/blob/HEAD/Readme.md
Source Code:https://github.com/bottiger/SoundWaves
Issue Tracker:https://github.com/bottiger/SoundWaves//issues

Auto Name:SoundWaves
Summary:Manage and listen to podcasts
Description:
Manage, fetch and listen to podcasts.

[https://github.com/bottiger/SoundWaves/blob/HEAD/Changelog.txt Changelog]
.

Repo Type:git
Repo:https://github.com/bottiger/SoundWaves/

Build:0.69,83
    disable=fail at dex
    commit=2ef1cfcff49916cb72f8acc4f088e762dd09a973
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' -e '/jsoup/afreeCompile "ch.acra:acra:4.5.0"' build.gradle

Build:0.70,84
    commit=v0.84
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.72,86
    commit=86
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.74,88
    commit=88
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.76,92
    commit=92
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.78.1,96
    commit=96
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.78.2,98
    commit=98
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Build:0.80,100
    commit=100
    subdir=app
    gradle=free
    rm=app/src/amazon/libs/*.jar
    prebuild=sed -i -e '/amazonCompile/d' -e '/googleCompile/d' build.gradle && \
        sed -i -e 's/javaMaxHeapSize "4g"/javaMaxHeapSize "2g"/g' build.gradle

Auto Update Mode:Version %c
Update Check Mode:Tags
Current Version:0.80
Current Version Code:100

